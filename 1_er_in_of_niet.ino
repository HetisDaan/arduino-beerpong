// Define Trig and Echo pin:

int trigPins[10] = {38, 36, 34, 32, 30, 5, 7, 9, 11, 12};
int echoPins[10] = {39, 37, 35, 33, 31, 4, 6, 8, 10, 13};

// Define variables:
long duration = 0;
int distance = 0;
int cupsFull = 0;

boolean cups[10] = {false,false,false,false,false,false,false,false,false,false};


void setup() {
  // Define inputs and outputs:
  

  for(int i = 0; i < 10; i++) 
  {
    pinMode(trigPins[i], OUTPUT);
    pinMode(echoPins[i], INPUT);
    digitalWrite(trigPins[i], LOW);
  }

  //Begin Serial communication at a baudrate of 9600:
  Serial.begin(9600);
}

void loop(){
  if (cupsFull == 10)
  {
    //game over, reset all cups
    cupsFull = 0;
    for(int i = 0; i < 10; i++) 
    {
      cups[i] = false;
    }
  }

  
  for(int i = 0; i < 10; i++) 
  {
    if (cups[i] == false)
    {

      sendPing(trigPins[i]);
      int duration = pulseIn(echoPins[i], HIGH, 2000);
      if (duration == 0) continue;
      distance = 0.034 * duration / 2;
      distance = clamp(distance, 0, 50);
      if (distance < 12)
      {
        cupsFull++;
        cups[i] = true;
        Serial.print(i);
        Serial.println("=in");
      }
    }
    
    
  }

//  delay(50);
}

void sendPing(int sensor){
 digitalWrite(sensor, HIGH);
 delayMicroseconds(10);
 digitalWrite(sensor, LOW);
}

int clamp(int VAL, int min, int max){
    if(VAL <= min){
    VAL = min;
    return VAL;
    }
    else if(VAL >= max){
    VAL = max;
    return VAL;
    }
    else{
    return VAL;
    }
}
