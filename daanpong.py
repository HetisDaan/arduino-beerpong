from stupidArtnet import StupidArtnet
import time
import random
import serial.tools.list_ports

ports = serial.tools.list_ports.comports()

for port, desc, hwid in sorted(ports):
        print("{}: {} [{}]".format(port, desc, hwid))

comPort = input("Please select a com port...")

print("You chose: ", comPort);

target_ip = '2.20.95.8'
universe = 0
packet_size = 512
led_count = 64

class LedColors:
    RED = [255, 0, 0]
    GREEN = [0, 255, 0]
    BLUE = [0, 0, 50]
    DARKRED = [255, 0, 0]
    MAGENTA = [255, 0, 255]


CUPCOLOR_ON = LedColors.GREEN
CUPCOLOR_OFF = LedColors.DARKRED
# Array of cup positions
CupArray = [
    [32, 33, 36, 37],
    [34, 35, 38, 39],
    [0, 1, 4, 5],
    [2, 3, 6, 7], 
    [41, 42, 45, 46],
    [43, 8, 47, 12],
    [9, 10, 13, 14],
    [50, 51, 54, 55],
    [16, 17, 20, 21],
    [59, 24, 63, 28]
]

# LED helper function
def SetLed(ledData: bytearray, led: int, color: list):
    startAddr = (led * 3)
    ledData[startAddr] = color[0]
    ledData[startAddr+1] = color[1]
    ledData[startAddr+2] = color[2]

def ClearLed(ledData: bytearray, led: int):
    startAddr = (led * 3)-2
    ledData[startAddr] = 0
    ledData[startAddr+1] = 0
    ledData[startAddr+2] = 0


def SetCupOn(cup: int, packet):
    for i in range(4):
        if CupArray[cup][i] >= 0:
            SetLed(packet, CupArray[cup][i], CUPCOLOR_ON)
    
def SetCupOff(cup: int, packet):
    for i in range(4):
        if CupArray[cup][i] >= 0:
            SetLed(packet, CupArray[cup][i], CUPCOLOR_OFF)


a = StupidArtnet(target_ip, universe, packet_size, 40, True, True)

print(a)

# Create artnet packet
packet = bytearray(packet_size)
for i in range(packet_size):
    # Initialize it to zero for sure
    packet[i] = 0

# Set it just to be sure
a.set(packet)

# Start ArtNet Sending thread
a.start()

# open com port
ser = serial.Serial(comPort, 9600, timeout=0.1)

# Set base color (where no cups) at the moment blue
for x in range(led_count):
    SetLed(packet, x, LedColors.BLUE)

# Set first color for the cups
for x in range(10):
    SetCupOff(x, packet)
    a.set(packet)

cupCount = 0

# As long as not all cups are in, keep looking for new cups
# After that, end game.
while cupCount < 10:
    line = ser.readline()   # read a '\n' terminated line
    if (len(line) > 0):
        lineString = line.decode('utf-8')
        if "=in" in lineString:
            turnCupOn = (int)(lineString[0])
            print('Turning cup on: ', turnCupOn)
            SetCupOn(turnCupOn, packet)
            cupCount += 1
    a.set(packet)


input("Press Enter to end game...")

# ArtNet controller holds the last value, so clear the data before exiting
a.blackout()

# Close the ArtNet sending thread before closing
a.stop()

# cleanup
del a